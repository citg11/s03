package com.zuitt;

import java.io.IOException;
import java.io.PrintWriter;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

public class CalculatorServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public void init() throws ServletException {
		System.out.println("******************************************");
		System.out.println(" CalculatorServlet has been initialized. ");
		System.out.println("******************************************");
	}
	
	public void doGet(HttpServletRequest req, HttpServletResponse res) throws IOException {
		PrintWriter out = res.getWriter();
		out.println("<h1>You are now using the calculator app</h1>");
		out.println("To use the app, input two number and an operation.<br>");
		out.println("Hit submit button after filling in the details.<br>");
		out.println("You will get the result shown in your browser?");
    }
	

	public void doPost(HttpServletRequest req, HttpServletResponse res) throws IOException {
		int num1 = Integer.parseInt(req.getParameter("num1"));
		int num2 = Integer.parseInt(req.getParameter("num2"));
		String operation = req.getParameter("operation");
				
		int ans;
		switch(operation) {
		case "add":
			ans = num1+num2;
			PrintWriter out = res.getWriter();
			out.println("The two numbers you provided are: " + num1 + "," + num2 +"\n");
			out.println("The operation you wanted is: " +operation+ "\n");
			out.println("The result is: " +ans+ "\n");
			break;
		case "subtract":
			ans = num1 - num2;
			PrintWriter out1 = res.getWriter();
			out1.println("The two numbers you provided are: " + num1 + "," + num2 +"\n");
			out1.println("The operation you wanted is: " +operation+ "\n");
			out1.println("The result is: " +ans+ "\n");
			break;
		case "multiply":
			ans = num1 * num2;
			PrintWriter out2 = res.getWriter();
			out2.println("The two numbers you provided are: " + num1 + "," + num2 +"\n");
			out2.println("The operation you wanted is: " +operation+ "\n");
			out2.println("The result is: " +ans+ "\n");
			break;
		case "divide":
			ans = num1 / num2;
			PrintWriter out3 = res.getWriter();
			out3.println("The two numbers you provided are: " + num1 + "," + num2 +"\n");
			out3.println("The operation you wanted is: " +operation+ "\n");
			out3.println("The result is: " +ans+ "\n");
			break;
		}
		
	
	}
	public void destroy(){
		System.out.println("******************************************");
		System.out.println(" CalculatorServlet has been destroy. ");
		System.out.println("******************************************");
	}
	

}
